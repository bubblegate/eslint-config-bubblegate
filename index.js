module.exports = {
	"env": { "es6": true },
	"parserOptions": { "ecmaVersion": "2017" },
	"plugins": ["security"],
	"extends": [
		"airbnb-base",
		"plugin:security/recommended"
	],
	"rules": {
		"comma-dangle": "off",
		"indent": ["error", "tab"],
		"no-continue": "off",
		"no-param-reassign": "off",
		"no-tabs": "off",
		"no-plusplus": "off",
		"object-curly-newline": ["error", { "multiline": true, "consistent": true }],
		"one-var": ["error", { "initialized": "never" }],
		"one-var-declaration-per-line": ["error", "initializations"],
		"import/extensions": ["error", "ignorePackages", { "js": "never", "json": "never" }],
		"import/first": ["error", { "absolute-first": false }],
		"import/no-amd": "error",
		"import/no-commonjs": "error",
		"import/order": ["error", { "newlines-between": "never" }]
	}
};
